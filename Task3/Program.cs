﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a number:");
            int number = int.Parse(Console.ReadLine());

            bool isSimple = true;
            //Очевидно, постановка задачи некорректна, нужно перебирать числа, начиная с числа 2,
            //т.к. любое число разделится на 1 без остатка
            int i = 2;
            while (i < number)
            {
                if (number % i == 0)
                {
                    isSimple = false;
                    break;
                }

                i++;
            }

            Console.WriteLine(isSimple ? "Simple" : "Not simple");
            Console.ReadKey(true);
        }
    }
}
