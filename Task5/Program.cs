﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input end of diapason:");
            int max = int.Parse(Console.ReadLine());

            Random random = new Random();
            int number = random.Next(max);
            int attempts = 0;

            while (true)
            {
                attempts++;
                Console.WriteLine("Try to guess a number:");
                string input = Console.ReadLine();
                if (input == "")
                {
                    Console.WriteLine($"Game over. Correct number was {number}");
                    break;
                }

                int inputNumber = int.Parse(input);
                if (inputNumber > number)
                {
                    Console.WriteLine("Your number is greater than needed.");
                }
                else if (inputNumber < number)
                {
                    Console.WriteLine("Your number is lesser than needed.");
                }
                else
                {
                    Console.WriteLine($"Congratulations! You guessed. The correct number is {number}");
                    Console.WriteLine($"Attempts count: {attempts}");
                    Console.WriteLine("Started new game!");
                    number = random.Next(max);
                    attempts = 0;
                }
            }
        }
    }
}
