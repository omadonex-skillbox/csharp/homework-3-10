﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello. Please input count of cards:");
            int count = int.Parse(Console.ReadLine());

            int sum = 0;
            for (int i = 0; i < count; i++)
            {               
                Console.WriteLine("Input a card:");
                string card = Console.ReadLine();
                //Очевидно, что в default могут попасть и значения, например 11, 12 и т.д. и строки типа "fsdgs"
                //Но мы ни в одном задании до этого не делали проверку на корректность данных введенных пользователем
                //И нигде не было защиты от ввода вместо числа, какой нибудь некорректной строки.
                //Поэтому данный switch отлично справится с условием задачи.
                switch (card)
                {
                    case "K":
                    case "Q":
                    case "J":
                    case "T":
                        sum += 10;
                        break;
                    default:
                        sum += int.Parse(card);
                        break;
                }               
            }

            Console.WriteLine($"Total sum of cards: {sum}");
            Console.ReadKey(true);
        }
    }
}
