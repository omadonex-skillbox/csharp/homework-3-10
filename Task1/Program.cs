﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a number:");
            int number = int.Parse(Console.ReadLine());
            bool isEven = number % 2 == 0;
            Console.WriteLine(isEven ? "Even" : "Odd");

            Console.ReadKey(true);
        }
    }
}
