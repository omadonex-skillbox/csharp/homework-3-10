﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input sequence length:");
            int length = int.Parse(Console.ReadLine());

            int min = int.MaxValue;
            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Input a number:");
                int number = int.Parse(Console.ReadLine());
                if (number < min)
                {
                    min = number;
                }
            }

            Console.WriteLine($"Minimal number is {min}");
            Console.ReadKey(true);
        }
    }
}
